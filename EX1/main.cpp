#include <iostream>
#include "bst.h"

int main () 
{
	
	BSNode<string>* bs = new BSNode<string>("6");
	bs->insert("2");
	bs->insert("8");
	bs->insert("3");
	bs->insert("5");
	bs->insert("9");
	bs->insert("6");
	bs->deleteN("2");
	bs->printWords();

	BSNode<int>* bsi = new BSNode<int>(5);
	bsi->insert(2);
	bsi->insert(8);
	bsi->insert(3);
	bsi->insert(5);
	bsi->insert(9);
	bsi->insert(6);
	bsi->deleteN(2);
	bsi->printWords();
}

